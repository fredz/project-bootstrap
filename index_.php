<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Html5 encode using bootstrap, Lesscss"/>
    <meta name="keywords" content=""/>
    <meta name="author" content="Owen Peredo Diaz"/>
    <meta charset="UTF-8"/>
    <link rel="icon" href="img/favicon.ico" />
    
    <title>Psd slice - html5</title>
    
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- End Bootstrap -->
    
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" media="all" href="css/less.php?file=style.less" />
    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.bxslider.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        jQuery(function()
        {
            $("nav ul, header .language").each(function(){ $(this).children("li:last").addClass("last"); });
            $("#main_content .slider ul").bxSlider({controls: false});
        });
    </script>
    
    <!--[if lt IE 9]>
    	<script src="js/html5.js"></script>
        
    <![endif]-->
</head>

<body>

    <div class="wraper container-fluid">
        <!-- START HEADER -->
        <header class="row-fluid">
            <div class="span3">
                <h1><a href="#" title=""><img alt="" src="img/logo.png" /></a></h1>
            </div>
            <div class="span9">
                <div class="row-fluid">
                    <ul class="language span3">
                        <li><a href="#" title="">ES</a></li>
                        <li><a href="#" title="">CA</a></li>
                    </ul>
                    
                    <nav class="span9 topmenu">
                        <ul>
                            <li><a href="#" title="">Faqs</a></li>
                            <li><a href="#" title="">Nota legal</a></li>
                            <li><a href="#" title="">Bolsa de trabajo</a></li>
                            <li><a href="#" title="">Mapa web</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="row-fluid">
                    <div class="span9 slogan">
                        Centre de diagnostic per la imatge
                    </div>
                    <div class="span3 text-right">
                        <h2><a href="#" title=""><img alt="" src="img/logo2.png" /></a></h2>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER -->
        
        <!-- START CONTENT -->
        <section id="main_content" class="row-fluid">
            
            <!-- START SIDEBAR -->
            <aside class="span2">
                <div class="aside_w">
                    <nav>
                        <ul>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                        </ul>
                    </nav>
                    
                    <div class="search text-center margin-top40">
                        <h4 class="text-left">Buscador</h4>
                        <form action="#" method="get">
                            <input type="text" name="" value="" placeholder="" />
                            <button type="submit">Buscar</button>
                        </form>
                    </div>
                    
                    <ul class="banners">
                        <li>
                            <a href="#" title=""><img alt="" src="img/cita.png" /></a>
                        </li>
                        <li>
                            <a href="#" title=""><img alt="" src="img/banner2.png" /></a>
                        </li>
                        <li>
                            <a href="#" title=""><img alt="" src="img/banner3.png" /></a>
                        </li>
                    </ul>
                </div>
            </aside>
            <!-- END SIDEBAR -->
            
            
            <!-- START PAGE -->
            <div class="w_page_content span10">
                <div class="page_content home">
                    <div class="slider">
                        <ul>
                            <li> <img alt="" src="img/slider1.png" /> </li>
                            <li> <img alt="" src="img/slider1.png" /> </li>
                            <li> <img alt="" src="img/slider1.png" /> </li>
                            <li> <img alt="" src="img/slider1.png" /> </li>
                        </ul>
                    </div>
                    <div class="last_posts">
                        <div class="row-fluid">
                            <?php for($i = 0; $i<3; $i++): ?>
                                <div class="span4">
                                    <div class="item">
                                        <h4>cuadro medico</h4>
                                        <div class="pimg">
                                            <img alt="" src="img/img1.png" />
                                        </div>
                                        <div class="descr">
                                            El centro de Argiles| vives dispone de los mejores medicos para su diagnostico por la imagen. U grupo de especialistas que... <a class="more" href="#" title="">Leer mas &gt;</a> 
                                        </div>
                                    </div>
                                </div>
                            <?php endfor ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE -->
            
        </section>
        <!-- END CONTENT -->
    </div>
    <!-- START FOOTER -->
    <footer class="row-fluid">
        <div class="wrapper">
            <p class="text-center span8 offset2">
                C.M. DELFOS: Av. Vallcarca 151-156, 08026 Barcelona | C.M. DELFOS: Av. Vallcarca 151-156, 08026 Barcelona | C.M. DELFOS: Av. Vallcarca 151-156, 08026 Barcelona | C.M. DELFOS: Av. Vallcarca 151-156, 08026 Barcelona
            </p>
        </div>
    </footer>
    <!-- END FOOTER -->
    
</body>
</html>