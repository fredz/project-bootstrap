<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Html5 encode using bootstrap, Lesscss"/>
    <meta name="keywords" content=""/>
    <meta name="author" content="Owen Peredo Diaz"/>
    <meta charset="UTF-8"/>
    <link rel="icon" href="img/favicon.ico" />
    
    <title>Psd slice - html5</title>
    
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- End Bootstrap -->
    
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" media="all" href="css/less.php?file=style.less" />
    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.bxslider.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        jQuery(function()
        {
            $("nav ul, header .language").each(function(){ $(this).children("li:last").addClass("last"); });
            $("#main_content .slider ul").bxSlider({controls: false});
        });
    </script>
    
    <!--[if lt IE 9]>
    	<script src="js/html5.js"></script>
        
    <![endif]-->
</head>

<body>

    <div class="wraper container-fluid">
        <!-- START HEADER -->
        <header class="row-fluid">
            <div class="span3">
                <h1><a href="#" title=""><img alt="" src="img/logo.png" /></a></h1>
            </div>
            <div class="span9">
                <div class="row-fluid">
                    <ul class="language span3">
                        <li><a href="#" title="">ES</a></li>
                        <li><a href="#" title="">CA</a></li>
                    </ul>
                    
                    <nav class="span9 topmenu">
                        <ul>
                            <li><a href="#" title="">Faqs</a></li>
                            <li><a href="#" title="">Nota legal</a></li>
                            <li><a href="#" title="">Bolsa de trabajo</a></li>
                            <li><a href="#" title="">Mapa web</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="row-fluid">
                    <div class="span9 slogan">
                        Centre de diagnostic per la imatge
                    </div>
                    <div class="span3 text-right">
                        <h2><a href="#" title=""><img alt="" src="img/logo2.png" /></a></h2>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER -->
        
        <!-- START CONTENT -->
        <section id="main_content" class="row-fluid">
            
            <!-- START SIDEBAR -->
            <aside class="span2">
                <div class="aside_w">
                    <nav>
                        <ul>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                            <li><a href="#" title="">Quienes somos</a></li>
                        </ul>
                    </nav>
                    
                    <div class="search text-center margin-top40">
                        <h4 class="text-left">Buscador</h4>
                        <form action="" method="">
                            <input type="text" name="" value="" placeholder="" />
                            <button type="submit">Buscar</button>
                        </form>
                    </div>
                    
                    <ul class="banners">
                        <li>
                            <a href="#" title=""><img alt="" src="img/cita.png" /></a>
                        </li>
                        <li>
                            <a href="#" title=""><img alt="" src="img/banner2.png" /></a>
                        </li>
                        <li>
                            <a href="#" title=""><img alt="" src="img/banner3.png" /></a>
                        </li>
                    </ul>
                </div>
            </aside>
            <!-- END SIDEBAR -->
            
            
            <!-- START PAGE -->
            <div class="w_page_content span10">
                <div class="page_content internal">
                    <div class="row-fluid">
                        <div class="span4 second_menu">
                            <h3 class="uppercase margin-left15">Equipamiento</h3>
                            <nav>
                                <ul>
                                    <li>
                                        <span href="#" title="">Resonancia magnetica</span>
                                        <ul>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span href="#" title="">Resonancia magnetica</span>
                                        <ul>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span href="#" title="">Resonancia magnetica</span>
                                        <ul>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span href="#" title="">Resonancia magnetica</span>
                                        <ul>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span href="#" title="">Resonancia magnetica</span>
                                        <ul>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                            <li><a href="#" title="">Equipo de resonancia magnetica</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="span8 internal_content">
                            <div class="w_internal_content">
                                <div class="p_breadcrumb"><a href="#">Equipamiento</a> � <a href="#">Equipamiento</a></div>
                                <article>
                                    <h1>Lo ultimo en diagnostico por la imagen</h1>
                                    <img alt="" src="img/internal.png" />
                                    <h2 class="uppercase">Introduccion</h2>
                                    <div class="content">
                                        El centro de Argiles| vives dispone de los mejores medicos para su diagnostico por la imagen. U grupo de especialistas que
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- END PAGE -->
            
        </section>
        <!-- END CONTENT -->
    </div>
    <!-- START FOOTER -->
    <footer class="row-fluid">
        <div class="wrapper">
            <p class="text-center span8 offset2">
                C.M. DELFOS: Av. Vallcarca 151-156, 08026 Barcelona | C.M. DELFOS: Av. Vallcarca 151-156, 08026 Barcelona | C.M. DELFOS: Av. Vallcarca 151-156, 08026 Barcelona | C.M. DELFOS: Av. Vallcarca 151-156, 08026 Barcelona
            </p>
        </div>
    </footer>
    <!-- END FOOTER -->
    
</body>
</html>